# opam-memcad-repository

This is an [opam](https://opam.ocaml.org) repository that contains
packages, that `memcad` depends from, maintaining package development
versions, before they are released in `opam` official repository.

To activate this repository in your current `opam` switch:
```
opam repo add memcad git+https://gitlab.inria.fr/memcad/opam-memcad-repository.git
```

This will add the `memcad` remote as a non-default extra source of opam
packages.

To create a new switch and activate this repository directly in it:
```
opam switch create 4.14.0 --repo=default,memcad=git+https://gitlab.inria.fr/memcad/opam-memcad-repository.git
```

## Versions and priority

For each package `$PACKAGE_NAME`, the subdirectory
`packages/$PACKAGE_NAME` contains one subdirectory
`packages/$PACKAGE_NAME/$PACKAGE_NAME.$VERSION` for each version. When
performing an install or update of a package, `opam` prefers the
greatest available version. If two repositories provide the same
version, a priority rank can be set between repositories using the
`opam repo` command. However, we prefer to disambiguate by adjusting
the version number directly.

To prioritize the package versions distributed in this repository over
the versions in the official opam repository, the version numbers are
incremented, typically by adding the suffix `.1` to the actual version
number. For example, `mlgmlidl.1.2.15.1` is preferred over the
official version `mlgmlidl.1.2.15`.

## Packages

### `bdd`

This repository contains the version `bdd.0.3.1`, which points to
[`ocaml-bdd` master](https://github.com/backtracking/ocaml-bdd/). The
version `bdd.0.3` packaged in the official `opam` repository does not
support OCaml 5.0.

### `setr`

This repository contains the version `setr.0.1.1.1`, which points to
the branch [`fix.ocaml5`](https://github.com/thierry-martinez/SETr/tree/fix.ocaml5)
([pull-request](https://github.com/arlencox/SETr/pull/2)). The
version `setr.0.1.1` packaged in the official `opam` repository does not
support OCaml 5.0.
